# Nodes Tutorial... Ignore the bad formatting, and bad explanation. I will professionalize later

To define a node, call 
```
minetest.register_node({"modname:node",
	description = "node name",
	tiles = {"texturename.png", "moretexturename.png"},
	groups = {groupshere=place in group here?}
	sounds = namespace.for.sounds()
	drop =  "modname:itemname"
})
```

It seems Lua's nodes/object system enumerates your nodes via `minetest.registered_nodes`, with `node.name` being the argument... Whatever Node and Name means, I think node is the `["Node_Here"] = {definition value}` and Name is actually the description string. Not guaranteed though.
Node groups are how you give nodes a list of predefined properties. I think.

There are oodles of node properties, some in-game, some in-mod, some elusive and undocumented. Separate from groups. 

Sounds are registered separately. I should probably write a guide on how to give your nodes sounds. Not sure not including sounds will make it silent or crash the game or something other then that. Default sounds will work. Called by `default.node_sound_here()`

Tiles is textures. Can have unlimited... No idea how they're wrapped, will have to experiment.

Default groups exist, didn't get a list of what those are.

There's got to be a way to give a node a model object, I'll update you when I find it.

# This will not be comprehensive, sorry, there are actual undocumented methods. Sling criticism into either issues or pull requests. Or let me know of undocumented methods I missed
